import Pomodoro from '../../js/components/Pomodoro.js'
import Bus from '../../js/libraries/Bus.js'

describe("Pomodoro component", () => {
  let document, bus, container = null

  beforeEach(() => {
    document = new FakeDocument()
    bus = new Bus()
    container = { innerHTML: {} }
  })

  it("renders a timer in the given container", () => {
    let pomodoro = new Pomodoro(container, bus, document)

    pomodoro.start()

    expect(container.innerHTML).toContain("time")
  })

  it("starts at 00", () => {
    let pomodoro = new Pomodoro(container, bus, document)

    pomodoro.start()

    expect(container.innerHTML).toContain("00")
  })

  it("renders a start button", () => {
    let pomodoro = new Pomodoro(container, bus, document)

    pomodoro.start()

    expect(container.innerHTML).toContain("Start")
  })

  it('renders time left after pass time', () => {
    const realBus = new Bus()
    let pomodoro = new Pomodoro(container, realBus, document)

    realBus.publish('timer.timeLeft', { minutes: 24 })

    expect(container.innerHTML).toContain("24")
  })

  class FakeDocument {
    querySelector(_) {
      return {}
    }
  }
})
