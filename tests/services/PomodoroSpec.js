import Pomodoro from '../../js/services/Pomodoro.js'
import Bus from '../../js/libraries/Bus.js'
import Callback from '../Callback.js'

describe('Pomodoro service', () => {
  let bus = null

  beforeEach(() => {
    bus = new Bus()
  })

  it('subscribes to start event', () => {
    spyOn(bus, 'subscribe')

    const service = new Pomodoro(bus)

    expect(bus.subscribe).toHaveBeenCalledWith('timer.start_requested', aCallback())
  })

  it('on start, publishes start event with initial time left', () => {
    const callback = new Callback(bus, 'timer.start')
    const initialTimeLeft = { minutes: 25 }
    const service = new Pomodoro(bus)

    service.start()

    expect(callback.hasBeenCalledWith()).toEqual(initialTimeLeft)
  })

  it('does not send a negative time left', () => {
    const callback = new Callback(bus, 'timer.timeLeft')
    const zeroTimeLeft = { minutes: 0 }
    const service = new Pomodoro(bus)
    service.start()

    for (let times = 0; times < 100; times++) {
       service.calculateTimeLeft()
     }

    expect(callback.hasBeenCalledWith()).toEqual(zeroTimeLeft)
  })

  function aCallback() {
    return jasmine.any(Function)
  }
})
