import Timer from '../../js/views/Timer.js'

describe('Timer', () => {
  it('draws enabled start button when the timer does not have time left', () => {
    const properties = { minutes: 0 }
    const enabledStartButton = '<button id="start">Start</button>'
    const timer = new Timer()

    const template = timer.render(properties)

    expect(template).toContain(enabledStartButton)
  })

  it('draws disabled start button when the timer is running', () => {
    const properties = { minutes: 24 }
    const disabledStartButton = '<button id="start" disabled>Start</button>'
    const timer = new Timer()

    const template = timer.render(properties)

    expect(template).toContain(disabledStartButton)
  })
})
