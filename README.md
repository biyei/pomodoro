# Pomodoro Devscola

This is an project to learn JavaScript in the "Run" from Zero in Devscola.

The project consist in a Pomodoro timer where shows the time left in minutes.

## System requirements

- Firefox version 60 or higher.

## How to run the application

Just open the `index.html` file in a project's main folder with your Firefox.

## How to run the tests

Open the `tests.html` file in a project's main folder with your Firefox ;P
