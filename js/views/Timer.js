class Timer {
  constructor(document) {
    this.document = document
  }

  render(properties) {
    const minutes = properties.minutes

    return `
      <div id="container">
        <div id="buttons">
          ${this._startButton(minutes)}
        </div>
        <div id="timer">
          <div id="time">
            <span id="minutes">${this._format(minutes)}</span>
          </div>
          <div id="filler"></div>
        </div>
      </div>
    `
  }

  addCallbacks(callbacks) {
    this._addOnClickToStart(callbacks.startCountDown)
  }

  _format(units) {
    if (units < 10) {
      return "0" + units.toString();
    }

    return units.toString();
  }

  _startButton(countDown) {
    const disabledButton = `<button id="start" disabled>Start</button>`
    if (this._isRunning(countDown)) { return disabledButton }

    const enabledButton = `<button id="start">Start</button>`
    return enabledButton
  }

  _addOnClickToStart(callback) {
    const start = this.document.querySelector('#start')
    start.onclick = callback
  }

  _isRunning(countDown) {
    return (countDown > 0)
  }
}

export default Timer
