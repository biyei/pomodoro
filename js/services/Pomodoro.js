
class Pomodoro {
  constructor(bus) {
    this.bus = bus

    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.start_requested', this.start.bind(this))
    this.bus.subscribe('timer.askTimeLeft', this.calculateTimeLeft.bind(this))
  }

  start() {
    const message = { minutes: 25 }

    this.bus.publish('timer.start', message)
  }

  calculateTimeLeft() {
    const message = { minutes: 0 }

    this.bus.publish('timer.timeLeft', message)
  }
}

export default Pomodoro
