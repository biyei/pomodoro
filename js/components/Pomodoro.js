import Timer from '../views/Timer.js'

const POLLING_FRECUENCE = 60000
const DEFAULT_PROPERTIES = { minutes: 0, seconds: 0 }

class Pomodoro {
  constructor(container, bus, document) {
    this.container = container
    this.renderer = new Timer(document)
    this.bus = bus

    this._subscribe()
  }

  _subscribe() {
    this.bus.subscribe('timer.start', this._showCountDown.bind(this))
    this.bus.subscribe('timer.timeLeft', this._showCountDown.bind(this))
  }

  start() {
    const callbacks = { startCountDown: this._startCountDown.bind(this) }

    this._drawRenderer(DEFAULT_PROPERTIES)
    this.renderer.addCallbacks(callbacks)
  }

  _startCountDown() {
    this.bus.publish('timer.start_requested')
  }

  _showCountDown(message) {
    this._askCountDown()
    this._drawRenderer(message)
  }

  _askCountDown() {
    setTimeout(() => {
      this.bus.publish('timer.askTimeLeft')
    }, POLLING_FRECUENCE)
  }

  _drawRenderer(message) {
    this.container.innerHTML = this.renderer.render(message)
  }
}

export default Pomodoro
